<%-- 
    Document   : createAccount
    Created on : 13-Mar-2018, 09:39:40
    Author     : Nguyen Duy Dat
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Create</title>
        <s:head/>
    </head>
    <body>
        <h1>Create new account</h1>
        <s:form action="register">
            <s:textfield name="username" label="Username *"/>
            <s:label value="6-20 char"/>
            <s:password name="password" label="Password *"/>
            <s:label value="6-30 char"/>
            <s:password name="confirm" label="Confirm Password *"/>
            <s:label value="6-30 char"/>
            <s:textfield name="lastname" label="Last name *"/>
            <s:label value="6-30 char"/>
            <s:submit value="Create"/>
        </s:form><br/>
        <s:if test="%{exception.message.contains('duplicate')}">
            <font color="red"><s:property value="username"/> is existed!!!</font>
        </s:if>
    </body>
</html>
